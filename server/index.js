const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const app = express();
const port = 3000;
//app.get('/',(req,res) => res.send('hello World!'));
const multer = require('multer')
const upload = multer()

var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/items', { useNewUrlParser: true });

var itemSchema = mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    created: {
        type: Date,
        default: Date.now
    }

});

var Items = mongoose.model('Items', itemSchema);

app.use(express.json()) // for parsing application/json
app.use(express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded
// for parsing multipart/form-data
app.use(upload.array());
app.use(express.static('public'));

const clientFolderPath = path.join(__dirname, '..', 'client/');

app.use('/', express.static(clientFolderPath));



// get all tobdos
app.get('/api', (req, res) => {
    Items.find({}).then(function (items) {
        res.json(items);
    });

});

// get todo by Id
app.get('/api/:id', (req, res) => {
    const { id } = req.params;

    Items.findById(id).then(response => {
        res.json(response)
    }).catch(err => {
        res.json({
            error: "something went wrong"
        })
    })

});

// create todo
app.post('/api', (req, res) => {
    var newItem = new Items(req.body);
    newItem.save(function (err, book) {
        if (err) return console.error(err);
        console.log(newItem.title + " saved to item collection.");
    });
    res.send('hi');
});

// update todo by id
app.put('/api/:id', (req, res) => {

});

// delete todo by id
app.delete('/api/:id', (req, res) => {
    const { id } = req.params;

    //console.log(req.params)



    Items.deleteOne({ _id: req.params.id })
        .then(res.json({ message: 'Success' }));
    //
});



app.listen(port, () => console.log(`Example App listening on port ${port}!`));
